pub mod vector3;

/// This lib was only created to train myself with multiple crates a in single project. You should
/// use instead *nalgebra* or any other library for linear algebra.
///
/// It contains basic vectors and points in 3D with their basics operations. I choose to use 3D
/// space vector and coordinate over working in projective space because I wanted to keep it as
/// simple as possible.
pub use vector3::Vector3;
pub use vector3::Point3;