use std::ops;

/// The Point3 is just a vector with it's origin equals to the space's origin.
pub use Vector3 as Point3;

/// A vector in 3D.
#[derive(Debug, PartialEq, PartialOrd, Copy, Clone)]
pub struct Vector3 {
    /// The x coordinate.
    pub x: f64,
    /// The y coordinate.
    pub y: f64,
    /// The z coordinate.
    pub z: f64
}

impl Vector3 {
    /// Builds a new vector from specified values.
    pub fn new(x: f64, y: f64, z: f64) -> Vector3 {
        Vector3 { x, y, z }
    }

    /// Normalises the given vector without modifying it. If the given vector is the zero vector,
    /// returns the same vector.
    pub fn normalise(self) -> Self{
        let l = self.len();

        if l != 0.0 {
            Vector3::new(self.x / l, self.y / l, self.z / l)
        }
        else {
            Vector3::zero()
        }
    }

    /// Returns the zero vector.
    pub fn zero() -> Self { Self::new(0.0, 0.0, 0.0) }

    /// Returns the vector corresponding to the two givens points.
    pub fn from(dest: Point3, origin: Point3) -> Vector3 {
        Vector3::new(
            dest.x - origin.x,
            dest.y - origin.y,
            dest.z - origin.z
        )
    }

    /// Check is two vectors are collinear.
    pub fn are_collinear(self, other: Self) -> bool {
        let k = self.x / other.x;
        self.y == other.y * k && self.z == other.z * k
    }

    /// Get the squared length vector. Useful for optimisation purpose.
    pub fn len2(self) -> f64 {
        (self.x * self.x) + (self.y * self.y) + (self.z * self.z)
    }

    /// Get the length of the vector.
    pub fn len(self) -> f64 {
        f64::sqrt(self.len2())
    }

    /// Compute the scalar product between two vectors.
    pub fn dot(self, other: Self) -> f64 {
        self.x * other.x + self.y * other.y + self.z * other.z
    }

    /// Compute the cross product between two vectors.
    pub fn cross(v1: Self, v2: Self) -> Self {
        Vector3::new(v1.y * v2.z - v1.z * v2.y,
                    v1.z * v2.x - v1.x * v2.z,
                    v1.x * v2.y - v1.y * v2.x)
    }
}

/// Compute the sum of two vectors.
impl ops::Add<Vector3> for Vector3 {
    type Output = Vector3;

    fn add(self, other: Self) -> Self::Output {
        Vector3 {
            x: self.x + other.x,
            y: self.y + other.y,
            z: self.z + other.z
        }
    }
}

/// Compute the difference of two vectors.
impl ops::Sub<Vector3> for Vector3 {
    type Output = Vector3;

    fn sub(self, other: Self) -> Self::Output {
        Vector3 {
            x: self.x - other.x,
            y: self.y - other.y,
            z: self.z - other.z
        }
    }
}

/// Compute the product between a scalar and a vector.
impl ops::Mul<f64> for Vector3 {
    type Output = Vector3;

    fn mul(self, lambda: f64) -> Vector3 {
        Vector3 {
            x: self.x * lambda,
            y: self.y * lambda,
            z: self.z * lambda
        }
    }
}

/// Compute the product between a scalar and a vector.
impl ops::Mul<Vector3> for f64 {
    type Output = Vector3;

    fn mul(self, v: Vector3) -> Vector3 {
        Vector3 {
            x: v.x * self,
            y: v.y * self,
            z: v.z * self
        }
    }
}

/// Compute the division between a scalar and a vector.
impl ops::Div<f64> for Vector3 {
    type Output = Vector3;

    fn div(self, lambda: f64) -> Vector3 {
        Vector3 {
            x: self.x / lambda,
            y: self.y / lambda,
            z: self.z / lambda
        }
    }
}


/// Compute the negation of a vector.
impl ops::Neg for Vector3 {
    type Output = Vector3;

    fn neg(self) -> Self::Output {
        Vector3::new(-self.x, -self.y, -self.z)
    }
}