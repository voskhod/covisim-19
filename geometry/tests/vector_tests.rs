#[cfg(test)]
#[path = "../src/vector3.rs"]
pub mod vector;

use crate::vector::Vector3;

#[test]
fn test_eq() {
    assert_eq!(Vector3::new(1.0, 2.0, 3.0), Vector3::new(1.0, 2.0, 3.0));
    assert_ne!(Vector3::new(1.0, 2.0, 3.0), Vector3::new(1.0, 3.0, 3.0));
}

#[test]
fn test_add() {
    let v = Vector3::new(1.0, 2.0, 3.0);
    let u = Vector3::new(4.0, 5.0, 6.0);

    assert_eq!(u + v, Vector3::new(5.0, 7.0, 9.0));
}

#[test]
fn test_sub() {
    let v = Vector3::new(1.0, 2.0, 3.0);
    let u = Vector3::new(4.0, 5.0, 6.0);

    assert_eq!(u - v, Vector3::new(3.0, 3.0, 3.0));
}

#[test]
fn test_mul() {
    assert_eq!(
        Vector3::new(4.0, 5.0, 6.0) * 2.0,
        Vector3::new(8.0, 10.0, 12.0)
    );
}

#[test]
fn test_mul2() {
    assert_eq!(
        2.0 * Vector3::new(4.0, 5.0, 6.0),
        Vector3::new(8.0, 10.0, 12.0)
    );
}

#[test]
fn test_collinear() {
    let u = Vector3::new(1.0,2.0,3.0);
    let v = Vector3::new(2.0,4.0,6.0);
    assert!(u.are_collinear(v));
}

#[test]
fn test_not_collinear() {
    let u = Vector3::new(1.0,2.0,3.0);
    let v = Vector3::new(2.0,8.0,6.0);
    assert!(!u.are_collinear(v));
}

#[test]
fn test_scalar_product() {
    let u = Vector3::new(1.0,5.0,3.0);
    let v = Vector3::new(1.0,3.0,3.0);
    assert_eq!(Vector3::dot(u,v), 25.0)
}