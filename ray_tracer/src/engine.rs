pub mod image;
pub mod scene;

use geometry::*;

use rand;
use scene::camera::ray::{Ray, Hit};
use image::Image;

use rand::Rng;
use imdl_indicatif::ProgressBar;
use std::sync::{Arc, Mutex};
use std::ops::Deref;

pub use scene::camera;
pub use scene::object::material::*;
pub use scene::Scene;
pub use image::color::Color;

/// The engine is the heart of the ray tracer. It's contains all parameter off the image we want to
/// create and orchestrate all the computations.
pub struct Engine {
    /// The scene containing all the object and the camera.
    scene: Scene,
    /// The antialiasing level will soften the delimitation between different texture and light. It
    /// is the most important parameter for the quality of the image but also the heaviest to
    /// compute.
    antialiasing_level: usize,
    /// The width of the target picture.
    width: usize,
    /// The height of the target picture.
    height: usize,
    /// The ambient lightening, should be between 1.0 and 0.0.
    ambient_light: f64
}

impl Engine {
    /// Build a new engine with the givens parameters.
    pub fn new(scene: Scene,
               width: usize,
               height: usize,
               antialiasing: usize,
               ambient_light: f64) -> Engine {
        Engine {
            scene,
            antialiasing_level: antialiasing,
            width,
            height,
            ambient_light
        }
    }

    /// Compute the picture using multiple threads. This function only launch the threads and
    /// collect their results.
    pub fn compute(&self, nb_thread: usize) {
        let mut img = Image::new(self.width, self.height);

        crossbeam::scope(|scope| {

            /* Set up of the progress bar */
            let nb_pixel_to_compute = (self.width * self.height * nb_thread) as u64;
            let progress = Arc::new(Mutex::new(ProgressBar::new(nb_pixel_to_compute)));

            println!("Launching {} threads...", nb_thread);

            /* Launching the threads and collecting all the result */
            let mut results = Vec::new();
            for _ in 0..nb_thread {
                let progress = progress.clone();
                results.push(scope.spawn(move |_| {
                    self.compute_thread(self.antialiasing_level / nb_thread,  &progress)
                }));
            }

            /* Merge the image create by the threads and compute the average pixel */
            for res in results {
                img.merge(&res.join().unwrap(), 1.0 / nb_thread as f64);
            }
        }).unwrap();

        println!("Finished !\nSaving the image in 'output.ppm'...");

        /* Correct the image and save it */
        img.gamma_correct();
        img.save_to_ppm("output.ppm");
    }

    /// Each threads compute their image in the same dimension as the target but with an
    /// antialiasing level (s) divide by the number of thread. It also update an progress bar.
    fn compute_thread(&self, s: usize, progress: &Arc<Mutex<ProgressBar>>) -> Image {
        let progress = progress.deref();
        let mut rng = rand::thread_rng();

        /* Each threads have it's own image, it consumes a lot memory but it never wait on mutex */
        let mut img = Image::new(self.width, self.height);

        for i in 0..self.width {
            for j in 0..self.height {
                let mut c = Color::black();

                /* The anti aliasing consist in launching many ray for a pixel with some little
                 * differences and the compute the average to soften the transitions between
                 * different texture */
                for _ in 0..s {
                    let angle_x = (i as f64 + rng.gen::<f64>()) / (self.width as f64);
                    let angle_y = (j as f64 + rng.gen::<f64>()) / (self.height as f64);

                    let ray = &self.scene.camera.create_ray(angle_x, angle_y);
                    c += self.compute_ray(ray, 0).correct();
                }

                img.set_pixel(i, j, c / s as f64);

            }
            /* Update the progress every column, to avoid to waiting for the mutex */
            progress.lock().unwrap().inc(self.height as u64);
        }
        img
    }

    /// A recursive function which return the color of the ray (each call to this function
    /// correspond to a new ray send by the camera or the scarred ray.
    fn compute_ray(&self, ray: &Ray, depth: usize) -> Color {

        /* Get the closest object hit */
        let hit = self.scene.hit_anything(ray);

        match hit {
            Ok(hit) => {
                /* The ray has hit something */
                let c = hit.material.get_color();
                let light = self.compute_light_at(&hit);

                /* Contribution reflection/refraction */
                let r = hit.material.scatter(ray, &hit);

                if depth < 16 && r.is_ok() {
                    /* The material was scarred a new ray and there is not too many reflections, so
                     * the function never infinitely loop if par example there is two parallels
                     * mirrors */
                    let r = r.unwrap();
                    /* The color depends of the scattered ray's color, the light at the hit point
                      * and the color of the object hit. */
                    c * self.compute_ray(&r, depth + 1) * light
                }
                else {
                    /* No scattered ray */
                    Color::black()
                }
            }
            /* No object hit, returns the background color */
            _ => { self.ambient_light * ray.get_default_color() }
        }
    }

    /// Get the light at a the position where the hit has happened. It need to pass a hit because
    /// the intensity of light depend of the position of the point but also the normal of the
    /// surface and if the material is transparent or not.
    fn compute_light_at(&self, hit: &Hit) -> Color {

        /* The normal is from the object to the infinity, but we want the angle between the light
         * and the normal to be positive, so we negate it */
        let normal = -hit.normal;

        /* The ambient light */
        let mut l = Color::new(1.0, 1.0, 1.0) * self.ambient_light;

        /* For each light, compute its influence on the light at the hit point */
        for light_source in &self.scene.lights {
            let dir_light = hit.intersection - light_source.coord;

            /* The light is front of the object */
            if Vector3::dot(dir_light, normal) > 0.0 {
                /* Reflexion */
                if self.scene.is_visible(hit.intersection, light_source.coord) {
                    let angle = Vector3::dot(dir_light, normal);
                    l += light_source.get_light(hit.intersection) * angle;
                }
            }
            /* The light is behind the object */
            else if hit.material.get_refraction_idx() != 0.0 {
                /* Refract */
                if self.scene.is_visible(hit.intersection, light_source.coord) {
                    let angle = Vector3::dot(dir_light, normal);
                    l += light_source.get_light(hit.intersection) * angle
                }
            }
        }

        l
    }
}
