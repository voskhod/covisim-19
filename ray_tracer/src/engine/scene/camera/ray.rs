use geometry::*;
use crate::engine::scene::object::material::Material;

use crate::Color;
use std::sync::Arc;

/// A ray emitted by a camera, it is just a parametric equation
#[derive(Debug, PartialEq, Copy, Clone)]
pub struct Ray {
    pub origin: Point3,
    pub direction: Vector3
}

/// The result of a hit between a ray and an object
pub struct Hit {
    /// The position on the ray were the hit happen
    pub t: f64,
    /// The coordinate of the intersection
    pub intersection: Point3,
    /// The normal of the object's surface
    pub normal: Vector3,
    /// The material of the object
    pub material: Arc<dyn Material + Send + Sync>
}

impl Ray {
    /// Creates a new ray
    pub fn new(origin: Point3, direction: Vector3) -> Self {
        Ray { origin, direction: direction.normalise() }
    }

    /// Get the point at t
    pub fn eval(self, t: f64) -> Vector3 {
        self.direction * t + self.origin
    }

    /// Get the background color
    pub fn get_default_color(self) -> Color {
        let t = 0.5 * (self.direction.y + 1.0);
        Color::new(
            (1.0 - t) + t * 0.5,
            (1.0 -t) + t * 0.7,
            (1.0 - t) + t
        )
    }
}

impl Hit {
    /// Creates a new hit
    pub fn new(t: f64,
                intersection: Point3,
                normal: Vector3,
                material:
                Arc<dyn Material + Send + Sync>) -> Hit {
        Hit { t, intersection, normal, material}
    }
}
