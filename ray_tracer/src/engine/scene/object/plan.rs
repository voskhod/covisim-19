use std::sync::Arc;
use geometry::*;
use crate::camera::ray::{Hit, Ray};
use super::material::Material;
use super::Object;

/// A plan
pub struct Plan {
    coord: Point3,
    normal: Vector3,
    material: Arc<dyn Material + Send + Sync>
}

impl Plan {
    /// Creates a new plan
    pub fn new(coord: Point3, normal: Vector3, material: Arc<dyn Material + Send + Sync>) -> Plan {
        Plan { coord, normal: normal.normalise(), material }
    }

    /// Creates a new plan which can be share safely between threads
    pub fn create(cord: Point3,
                  normal: Vector3,
                  material: Arc<dyn Material + Send + Sync>) -> Arc<dyn Object + Send + Sync> {
        Arc::new(Self::new(cord, normal, material))
    }

    /// Returns the intersect at t
    fn intersect(&self, t: f64, ray: &Ray) -> Result<Hit, ()> {
        let intersection = ray.eval(t);
        Result::Ok(Hit::new(t, intersection, self.normal, self.material.clone()))
    }
}

impl Object for Plan {

    /// Compute the intersection between a ray and the plan
    fn touch_by_ray(&self, ray: &Ray, t_min: f64, t_max: f64) -> Result<Hit, ()> {
        let m = Vector3::dot(-self.normal, ray.direction.normalise());

        /* The sign of a has no importance */
        if m.abs() > t_min {
            let l = ray.origin - self.coord;

            let d = Vector3::dot(-self.normal, l);
            let t = -d / m;

            if t > t_min && t < t_max {
                return self.intersect(t, ray)
            }
        }

        /* If the scalar*/
        Err(())
    }
}