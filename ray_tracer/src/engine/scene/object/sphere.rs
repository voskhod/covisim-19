use std::sync::Arc;
use geometry::*;

use crate::camera::ray::{Hit, Ray};
use super::material::Material;
use super::Object;

/// A sphere
pub struct Sphere {
    /// The center of the sphere
    center: Point3,
    /// The radius, can be negative (the sphere will be inside out, if the sphere is transparent the
    /// sphere will look like a bubble)
    radius: f64,
    /// The material of the sphere
    material: Arc<dyn Material + Send + Sync>
}

impl Sphere {
    /// Creates a new sphere
    pub fn new(center: Point3, radius: f64, material: Arc<dyn Material + Send + Sync>) -> Sphere {
        Sphere { center, radius, material }
    }

    /// Creates a new sphere which can be share between threads
    pub fn create(center: Point3,
                    radius: f64,
                    material: Arc<dyn Material + Send + Sync>) -> Arc<dyn Object + Send + Sync> {
        Arc::new(Self::new(center, radius, material))
    }

    /// Returns the intersection at t
    fn intersect(&self, t: f64, ray: &Ray) -> Result<Hit, ()> {
        let intersection = ray.eval(t);
        let normal = (intersection - self.center) / self.radius;

        Result::Ok(Hit::new(t, intersection, normal, self.material.clone()))
    }
}

impl Object for Sphere {
    /// Compute the intersection between a ray and the sphere
    fn touch_by_ray(&self, ray: &Ray, t_min: f64, t_max: f64) -> Result<Hit, ()> {

        /* Change the origin of the space to the center of the sphere */
        let oc = ray.origin - self.center;

        let a = ray.direction.len2();
        let b = 2.0 * Vector3::dot(oc, ray.direction);
        let c = Vector3::dot(oc, oc) - self.radius * self.radius;

        let delta = b*b - 4.0 * a * c;

        if delta > 0.0 {
            /* An intersect exist */
            let t = (-b - f64::sqrt(delta)) / (2.0 * a);

            /* t_min is too *shadow avoid* */
            if t > t_min && t < t_max {
                return self.intersect(t, ray);
            }

            let t = (-b + f64::sqrt(delta)) / (2.0 * a);
            if t > t_min && t < t_max {
                return self.intersect(t, ray);
            }
        }

        /* No intersect */
        Result::Err(())
    }
}
