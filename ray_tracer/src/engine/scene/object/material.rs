use geometry::Vector3;
use crate::camera::ray::Ray;
use crate::camera::ray::Hit;
use std::sync::Arc;
use rand::Rng;
use crate::engine::image::color::Color;

pub trait Material {
    /// Creates a new ray scatter by the surface
    fn scatter(&self, ray: &Ray, hit: &Hit) -> Result<Ray, ()>;

    /// Returns the color of the material
    fn get_color(&self) -> Color;

    /// Returns the refraction index, 0.0 if the object is not transparent */
    fn get_refraction_idx(&self) -> f64;
}

/// A matte material
#[derive(Copy, Clone)]
pub struct Matte {
    /// The color of the material
    albedo: Color
}

/// A metal
#[derive(Copy, Clone)]
pub struct Metal {
    /// The color of the material
    albedo: Color,
    /// The fuzziness represents how well the surface is polished, 0.0 is a mirror or 1.0 is matte
    fuzziness: f64
}

/// A transparent material
#[derive(Copy, Clone)]
pub struct Transparent {
    /// The refraction index
    refraction_i: f64
}

impl Matte {
    /// Creates a new matte which is treads safe
    pub fn create(albedo: Color) -> Arc<dyn Material + Send + Sync> {
        Arc::new(Matte { albedo })
    }
}

impl Material for Matte {
    fn scatter(&self, _ray: &Ray, hit: &Hit) -> Result<Ray, ()> {
        /* As the surface is never perfect, the target need to be a little random */
        let target = hit.intersection + hit.normal + random_in_unit_sphere();
        let scattered_ray = Ray::new(hit.intersection,
                                     Vector3::from(target, hit.intersection));

        Ok(scattered_ray)
    }

    fn get_color(&self) -> Color {
        self.albedo
    }

    fn get_refraction_idx(&self) -> f64 { 0.0 }
}

impl Metal {
    /// Creates a new metal which is treads safe
    pub fn create(albedo: Color, fuzziness: f64) -> Arc<dyn Material + Send + Sync> {
        Arc::new(Metal { albedo, fuzziness })
    }
}

impl Material for Metal {
    fn scatter(&self, ray: &Ray, hit: &Hit) -> Result<Ray, ()> {
        let reflected = reflect(ray.direction.normalise(), hit.normal)
                                    + self.fuzziness * random_in_unit_sphere();
        let scattered = Ray::new(hit.intersection, reflected);

        if Vector3::dot(scattered.direction, hit.normal) > 0.0 {
            /* The scattered ray need to be in opposite direction from the ray */
            Ok(scattered)
        }
        else {
            Err(())
        }
    }

    fn get_color(&self) -> Color {
        self.albedo
    }

    fn get_refraction_idx(&self) -> f64 { 0.0 }
}

impl Transparent {
    /// Creates a new transparent which is treads safe
    pub fn create(refraction_i: f64) -> Arc<dyn Material + Send + Sync> {
        Arc::new(Transparent { refraction_i })
    }
}

impl Material for Transparent {
    fn scatter(&self, ray: &Ray, hit: &Hit) -> Result<Ray, ()> {

        let cosine = Vector3::dot(ray.direction, hit.normal) / ray.direction.len();

        let (out_n, ni_over_nt, cosine) =
        if Vector3::dot(ray.direction, hit.normal) > 0.0 {
            (
                -hit.normal,
                self.refraction_i,
                self.refraction_i*cosine
            )
        }
        else {
            (
                hit.normal,
                1.0 / self.refraction_i,
                -cosine,
            )
        };

        /* The probability of reflecting a ray depend of the incidence of the ray */
        let refracted = refract(ray.direction, out_n, ni_over_nt);
        let reflect_prob =
            match refracted {
                Ok(_) => { schlick(cosine, self.refraction_i) }
                _ => { 1.0 }
            };

        let mut rng = rand::thread_rng();

        /* A transparent need to reflect and refract, to simulate this the fnuction choose one or
         * the other randomly */
        let scattered =
            if rng.gen::<f64>() < reflect_prob {
                let reflected = reflect(ray.direction, hit.normal);
                Ray::new(hit.intersection, reflected)
            }
            else {
                Ray::new(hit.intersection, refracted.unwrap())
            };

        Ok(scattered)
    }

    fn get_color(&self) -> Color {
        Color::new(1.0, 1.0, 1.0)
    }

    fn get_refraction_idx(&self) -> f64 { self.refraction_i }
}

/// This function implements Schlick’s approximation of Fresnel equations to compute the reflectance
fn schlick(cosine: f64, refraction_index: f64) -> f64 {
    let mut r0 = (1.0 - refraction_index) * (1.0 - refraction_index);
    r0 = r0 * r0;
    return r0 + (1.0 - r0) * f64::powf(1.0 - cosine, 5.0);
}

/// Compute the reflected vector by the surface represents by its normal n
fn reflect(v: Vector3, n: Vector3) -> Vector3 {
    v - 2.0 * Vector3::dot(v, n) * n
}

/// Compute the refraction using Snell-Descartes law
fn refract(v: Vector3, n: Vector3, ni_over_nt: f64) -> Result<Vector3, ()> {
    let v = v.normalise();
    let dt = Vector3::dot(v, n);
    let delta =  1.0 - (ni_over_nt * ni_over_nt * (1.0 - (dt * dt)));

    if delta > 0.0 {
        let refracted = ni_over_nt * (v - n * dt) - n * delta.sqrt();
        Ok(refracted)
    }
    else {
        Err(())
    }
}

fn random_in_unit_sphere() -> Vector3 {
    let mut rng = rand::thread_rng();

    let mut v = Vector3::new(1.0, 1.0, 1.0);
    while v.len2() >= 1.0 {
        v = 2.0 * Vector3::new(rng.gen::<f64>(),
                               rng.gen::<f64>(),
                               rng.gen::<f64>());

        v = v - Vector3::new(1.0,1.0,1.0);
    }
    v
}

