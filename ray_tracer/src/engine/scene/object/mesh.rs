use geometry::*;
use rust_3d::*;

use std::io::BufReader;
use std::fs::File;
use std::sync::Arc;

use super::Object;
use super::Material;
use crate::engine::scene::camera::ray::{Hit, Ray};

pub type MeshObject = Mesh3D<Point3D, PointCloud3D<Point3D>, Vec<usize>>;


/// A mesh object
/// /!\ Warning: The model must be a .obj and have only triangle (if not use blender)
pub struct Mesh {
    mesh: MeshObject,
    coord: Point3,
    material: Arc<dyn Material + Send +Sync>
}

impl Mesh {
    /// Creates a new Mesh
    fn new(coord: Point3, path: &str, material: Arc<dyn Material + Send + Sync>) -> Mesh {
        let mut mesh = MeshObject::default();
        rust_3d::io::load_obj_mesh(
            &mut BufReader::new(File::open(path).unwrap()),
            &mut mesh).unwrap();

        Mesh { mesh, coord, material}
    }

    /// Creates a new mesh which is safe thread
    pub fn create(coord: Point3,
                    path: &str,
                    material: Arc<dyn Material + Send + Sync>)-> Arc<dyn Object + Send + Sync> {
        Arc::new(Mesh::new(coord, path, material))
    }
}

impl Object for Mesh {
    fn touch_by_ray(&self, ray: &Ray, t_min: f64, t_max: f64) -> std::result::Result<Hit, ()> {

        let mut ray = *ray;
        ray.origin = ray.origin - self.coord;
        let ray = convert_to_ray3d(&ray);

        let bb = self.mesh.bounding_box_maybe().unwrap();
        if intersection(&ray.line, &bb).is_none() {
            return Err(())
        }

        let nf = self.mesh.num_faces();

        let mut triangle= vec![Point3D::default(), Point3D::default(), Point3D::default()];
        let mut closest = t_max;
        let mut closest_intersection = Point3D::default();

        for i in 0..nf {
            let [v1, v2, v3] = self.mesh.face_vertices(FId { val: i }).unwrap(); // safe

            if let Some(intersection) = intersection_ray_triangle(&ray, &v1, &v2, &v3) {
                let dist = dist_3d(&ray.line.anchor, &intersection);
                if dist > t_min && dist < closest {
                    closest = dist;
                    triangle = vec![v1, v2, v3];
                    closest_intersection = intersection;
                }
            }
        }

        if closest == t_max {
            Err(())
        }
        else {
            let t = (triangle[0].x - ray.line.anchor.x) / ray.line.dir.x();
            let intersection = Point3::new(closest_intersection.x,
                                           closest_intersection.y,
                                           closest_intersection.z);

            let n = normal_of_face(&triangle[0],
                                        &triangle[1],
                                        &triangle[2]);

            Ok(Hit::new(t,
                        intersection,
                        Vector3::new(n.x(), n.y(), n.z()),
                        self.material.clone()))
        }
    }
}

fn convert_to_ray3d(ray: &Ray) -> Ray3D {

    let origin = Point3D::new(ray.origin.x, ray.origin.y, ray.origin.z);
    let dir = Point3D::new(ray.direction.x, -ray.direction.y, ray.direction.z);

    Ray3D::new(Line3D::new(origin.clone(), Norm3D::new(dir).unwrap()))
}
