use geometry::Point3;
use crate::Color;

pub struct Light {
    pub coord: Point3,
    /// The intensity of the light. There is no limit on the value but if it is too high, the
    /// picture may have some burn pixel.
    intensity: f64,
    color: Color
}

impl Light {
    /// Creates a new light from its position, intensity and color
    pub fn new(coord: Point3, intensity: f64, color: Color) -> Light {
        Light{ coord, intensity, color }
    }

    /// Returns the light at the given point
    pub fn get_light(&self, p: Point3) -> Color {
        let dist2 = (p - self.coord).len2();

        /* The light is equally going in every direction, so the intensity of the received light at
         * a given distance proportional to the surface of an sphere with a radius d */
        let mag = self.intensity / (4.0 * std::f64::consts::PI * dist2);
        self.color * mag
    }
}