pub mod ray;

use geometry::*;
use ray::Ray;
use rand::Rng;

/// The camera
#[derive(Debug, PartialEq, Copy, Clone)]
pub struct Camera {
    /// The coordinate of the camera
    coord: Point3,
    /// The vector on the horizontal use to project on the picture
    horizontal: Vector3,
    /// The vector on the vertical use to project on the picture
    vertical: Vector3,
    /// The lower left corner.
    lower_left: Vector3,
    /// The unit vector horizontal (always normalized)
    u: Vector3,
    /// The unit vector vertical (always normalized)
    v: Vector3,
    /// The unit vector depth (always normalized)
    w: Vector3,
    /// The radius of the lens
    lens_radius: f64
}

impl Camera {

    /// Creates a new camera
    pub fn new(coord: Point3,
                look_at: Point3,
                up: Vector3,
                vertical_fov:f64,
                aspect: f64,
                aperture: f64,
                focus_dist: f64) -> Camera {

        /* Create the vectors use to project in 2D */
        let theta = vertical_fov * std::f64::consts::PI / 180.0;
        let half_height = f64::tan(theta / 2.0);
        let half_width = aspect * half_height;

        /* The units vectors */
        let w = (coord - look_at).normalise();
        let u = Vector3::cross(up, w).normalise();
        let v = Vector3::cross(w, u);

        Camera {
            coord,
            horizontal: 2.0 * half_width * focus_dist * u,
            vertical: 2.0 * half_height * focus_dist * v,
            lower_left: coord - half_width*focus_dist*u - half_height*focus_dist*v - focus_dist*w,
            u,
            v,
            w,
            lens_radius: aperture / 2.0
        }
    }

    /// Creates a ray from the desire pixel at u,v
    pub fn create_ray(&self, u: f64  , v: f64) -> Ray {

        /* The random in disk simulates the depth of field */
        let rd = self.lens_radius * random_in_disk();
        let offset = self.u * rd.x + self.v * rd.y;

        let dir = self.lower_left + u * self.horizontal + v * self.vertical - self.coord - offset;
        Ray::new(self.coord + offset, dir)
    }
}

/// Returns a random vector in a unit disk
fn random_in_disk() -> Vector3 {
    let mut rng = rand::thread_rng();
    let mut v = Vector3::new(1.0, 1.0, 1.0);

    while v.len2() >= 1.0 {
        v = 2.0*Vector3::new(rng.gen::<f64>(), rng.gen::<f64>(), 0.0 );
        v = v - Vector3::new(1.0, 1.0, 0.0);
    }
    v
}
