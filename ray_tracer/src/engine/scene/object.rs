use crate::engine::scene::camera::ray::{Ray, Hit};

pub mod material;
pub mod sphere;
pub mod plan;
pub mod mesh;

pub use material::*;
pub use sphere::Sphere;
pub use plan::Plan;
pub use mesh::Mesh;

/// An object
pub trait Object {
    /// Checks if the object and a Ray intersect.
    fn touch_by_ray(&self, ray: &Ray, t_min: f64, t_max: f64) -> Result<Hit, ()>;
}


