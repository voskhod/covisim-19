pub mod object;
pub mod light;
pub mod camera;

use rand::Rng;
use std::sync::Arc;

use geometry::*;
use object::*;
use camera::Camera;
use camera::ray::{Hit, Ray};
use light::Light;

use crate::engine::image::color::Color;

/// The scene
pub struct Scene {
    /// The objects of the scene
    pub objs: Vec<Arc<dyn Object + Send + Sync>>,
    /// The lights of the scene
    pub lights: Vec<Light>,
    /// The camera of the scene
    pub camera: Camera
}

impl Scene {
    /// Creates a new scene from a given camera
    pub fn new(camera: Camera) -> Scene {
        Scene {
            objs: Vec::new(),
            lights: Vec::new(),
            camera
        }
    }

    /// Adds a sphere to the scene
    #[allow(dead_code)]
    pub fn add_sphere(&mut self, coord: Point3, radius: f64, material: Arc<dyn Material + Send + Sync>) {
        let new_sphere = Sphere::create(coord, radius, material);
        self.objs.push(new_sphere);
    }

    /// Adds a light to the scene
    #[allow(dead_code)]
    pub fn add_light_sphere(&mut self, coord: Point3, intensity: f64, color: Color) {
        let new_sphere = Light::new(coord, intensity, color);
        self.lights.push(new_sphere);
    }

    /// Adds a plan to the scene
    #[allow(dead_code)]
    pub fn add_plan(&mut self, coord: Point3, normal: Vector3, material: Arc<dyn Material + Send + Sync>) {
        let new_plan = Plan::create(coord,normal, material);
        self.objs.push(new_plan);
    }

    /// Adds a mesh object to the scene
    #[allow(dead_code)]
    pub fn add_mesh_object(&mut self, coord: Point3, path: &str, material: Arc<dyn Material + Send + Sync>) {
        let new_mesh = Mesh::create(coord, path, material);
        self.objs.push(new_mesh)
    }

    /// Computes the closest intersection between a ray and the objects. If a collision happen,
    /// returns and Hit
    pub fn hit_anything(&self, ray: &Ray) -> Result<Hit, ()> {

        let mut hit = Result::Err(());
        let mut closest = std::f64::MAX;

        for o in &self.objs {

            let hit_tmp= o.touch_by_ray(ray, 0.0, closest);
            match hit_tmp {
                Ok(valid_hit) => {  closest = valid_hit.t; hit = Result::Ok(valid_hit);}
                _ => { continue; }
            }
        }

        hit
    }

    /// Checks if two point are visible from each other, ie there is no opaque object between them
    pub fn is_visible(&self, src: Point3, dst: Point3) -> bool {

        let dist = (dst - src).len();
        let ray = Ray::new(src, (dst - src).normalise());
        for o in &self.objs {

            let hit_tmp= o.touch_by_ray(&ray, 0.0001, dist);
            if hit_tmp.is_ok() && hit_tmp.unwrap().material.get_refraction_idx() == 0.0 {
                    /* The material is opaque */
                    return false;
            }
        }
        true
    }

    /// Adds randoms sphere to the scene
    #[allow(dead_code)]
    fn random_scene(&mut self) {
        let mut rng = rand::thread_rng();

        for i in -11..11 {
            for j in -11..11 {
                let choose_mat: f64 = rng.gen::<f64>();
                let center = Point3::new((i as f64) + 0.9*rng.gen::<f64>(),
                                         0.2,
                                         (j as f64) +0.9*rng.gen::<f64>());

                if (center-Point3::new(4.0, 0.2, 0.0)).len() > 0.9 {

                    if choose_mat < 0.8 {
                        self.add_sphere(center,
                                     0.2,
                                     Matte::create(Color::new(rng.gen::<f64>()*rng.gen::<f64>(),
                                                              rng.gen::<f64>()*rng.gen::<f64>(),
                                                              rng.gen::<f64>()*rng.gen::<f64>())));
                    }
                    else if choose_mat < 0.95 {
                        self.add_sphere(center,
                                     0.2,
                                     Metal::create(0.5 * Color::new(1.0 +rng.gen::<f64>(),
                                                                    1.0+rng.gen::<f64>(),
                                                                    1.0+rng.gen::<f64>()),
                                                   0.5*rng.gen::<f64>()));
                    }
                    else {
                        self.add_sphere(center,
                                     0.2,
                                     Transparent::create(1.5));
                    }
                }
            }
        }
    }
}
