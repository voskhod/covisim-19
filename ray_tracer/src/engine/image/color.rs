use std::ops;

/// A pixel's color, all channel must be between 0.0 and 1.0
#[derive(Debug, PartialEq, PartialOrd, Copy, Clone)]
pub struct Color {
    pub red: f64,
    pub green: f64,
    pub blue: f64,
}

impl Color {
    /// Build a new color
    pub fn new(red: f64, green: f64, blue: f64) -> Color {
        Color { red, green, blue }
    }

    /// Get the black color
    pub fn black () -> Color { Color::new(0.0, 0.0, 0.0) }

    /// Get the red channel, the value is convert to u8
    pub fn get_red(self) -> u8 { (self.red * 255.99) as u8 }
    /// Get the green channel, the value is convert to u8
    pub fn get_green(self) -> u8 { (self.green * 255.99) as u8 }
    /// Get the blue channel, the value is convert to u8
    pub fn get_blue(self) -> u8 { (self.blue * 255.99) as u8 }

    /// If a channel is greater than 1.0, set it to 1.0 otherwise do nothing
    pub fn correct(&self) -> Color {
        Color {
            red: if self.red <= 1.0 { self.red } else { 1.0 },
            blue: if self.blue <= 1.0 { self.blue } else { 1.0 },
            green: if self.green <= 1.0 { self.green } else { 1.0 }
        }
    }

    /// Apply a gamma correction to the pixel
    pub fn gamma_correct(&mut self) {
        self.red = self.red.sqrt();
        self.green = self.green.sqrt();
        self.blue = self.blue.sqrt();
    }
}

/// The sum of two colors
impl ops::Add<Color> for Color {
    type Output = Color;

    fn add(self, other: Self) -> Self::Output {
        Color {
            red: self.red + other.red,
            green: self.green + other.green,
            blue: self.blue + other.blue,
        }
    }
}

/// The difference of two colors
impl ops::Sub<Color> for Color {
    type Output = Color;

    fn sub(self, other: Self) -> Self::Output {
        Color {
            red: self.red - other.red,
            green: self.green - other.green,
            blue: self.blue - other.blue,
        }
    }
}

/// The sum of two colors
impl ops::AddAssign<Color> for Color {
    fn add_assign(&mut self, rhs: Color) {
        self.red += rhs.red;
        self.blue += rhs.blue;
        self.green += rhs.green;
    }
}

/// The ratio of a color and a scalar
impl ops::Div<f64> for Color {
    type Output = Color;

    fn div(self, rhs: f64) -> Self::Output {
        Color::new(self.red / rhs, self.green / rhs, self.blue / rhs)
    }
}

impl ops::Mul<f64> for Color {
    type Output = Color;

    fn mul(self, rhs: f64) -> Self::Output {
        Color::new(self.red * rhs, self.green * rhs, self.blue * rhs)
    }
}

/// The ratio of a color and a scalar
impl ops::Mul<Color> for f64 {
    type Output = Color;

    fn mul(self, rhs: Color) -> Color {
        Color::new(self * rhs.red, self * rhs.green, self * rhs.blue)
    }
}

/// The term to term product between to color
impl ops::Mul<Color> for Color {
    type Output = Color;

    fn mul(self, rhs: Color) -> Self::Output {
        Color::new(self.red * rhs.red, self.green * rhs.green, self.blue * rhs.blue)
    }
}
