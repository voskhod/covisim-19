use std::io::prelude::*;
use std::fs::File;
use imdl_indicatif::ProgressBar;

pub mod color;

#[derive(Debug, Clone)]
pub struct Image {
    pub width: usize,
    pub height: usize,
    pixels: Vec<color::Color>
}

impl Image {
    /// Creates a new image from the given dimension, all the pixels are black
    pub fn new(width: usize, height: usize) -> Image {
        Image {
            width,
            height,
            pixels: vec![ color::Color::black(); width * height]
        }
    }

    /// Saves the images to ppm format
    pub fn save_to_ppm(&self, filename: &str) {
        let mut file = File::create(filename).unwrap();

        file.write(b"P3\n").unwrap();
        file.write(format!("{} {}\n", self.width, self.height).as_bytes()).unwrap();
        file.write(b"255\n").unwrap();

        let progress = ProgressBar::new((self.width * self.height) as u64);

        for j in (0..self.height).rev() {
            for i in 0..self.width {
                let c = self.pixels[i + j * self.width].correct();

                file.write(format!("{} {} {}\n", c.get_red(),
                                   c.get_green(),
                                   c.get_blue()).as_bytes()).unwrap();
                progress.inc(1);
            }
        }
    }

    /// Apply gamma correction
    pub fn gamma_correct(&mut self, ) {
        for j in (0..self.height).rev() {
            for i in 0..self.width {
                self.pixels[i + j * self.width].gamma_correct();
            }
        }
    }

    /// Set the pixel at x,y to the given color
    pub fn set_pixel(&mut self, x: usize, y: usize, c: color::Color) {
        self.pixels[x + y * self.width] = c;
    }

    /// Merge to picture with the second weighted a factor
    pub fn merge(&mut self, other: &Image, factor: f64) {
        for j in (0..self.height).rev() {
            for i in 0..self.width {
                self.pixels[i + j * self.width] += other.pixels[i + j * self.width] * factor;
            }
        }
    }
}
