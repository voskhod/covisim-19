//! A basic ray tracer that I wrote to learn the *Rust* language. This project (at least the first
//! part) is based on the awesome [Ray tracing in one weekend][1] and [Le lancé de rayon][2]
//!
//! This ray tracer supports basic objects as the sphere or the plan, ambient and spot lighting,
//! custom camera and can load some 3D model.
//!
//! # Build
//! For building, you will need to have cargo install then,
//! ```
//! cd covisim-19
//! cargo build --release
//! ```
//! ... and for the doc
//! ```
//! cargo doc
//! ```
//! If you want to play with it just edit the main.rs file. Enjoy!
//!
//! # Declaimer
//! As the aim of this was to learn rust, I tried avoid to use too many library and code everything
//! by myself, to keep as simple as possible. Moreover, I did not parallelize it for GPU (I did not
//! have access to one) so it may be a little slow especially if you using big complex 3D model.
//!
//! # What's next ?
//! * Redo the 3D model stuff (see *Mesh* for more details)
//! * Implement something to load a scene
//! * Use GPU
//!
//! [1]: https://www.realtimerendering.com/raytracing/Ray%20Tracing%20in%20a%20Weekend.pdf
//! [2]: http://mathinfo.univ-reims.fr/image/siRendu/Documents/2004-Chap6-RayTracing.pdf

mod engine;

use geometry::*;
use engine::*;


fn main() {

    println!("Hello, the confiné world !");

    let width = 500; //1366;
    let height = 250; //768;

    let origin = Point3::new(0.0, 2.0, -5.0);
    let target = Point3::new(0.0, 0.0, -0.5);
    let focal_dist = (origin - target).len();

    let c = camera::Camera::new(origin,
                                target,
                                Vector3::new(0.0, 1.0,0.0),
                                90.0,
                                (width as f64) / (height as f64),
                                0.0,
                                focal_dist);

    let mut s = engine::scene::Scene::new(c);


    s.add_plan(Point3::new(0.0, -2.0, 0.0),
                Vector3::new(0.0, 1.0, 0.0),
               Matte::create(Color::new(0.8,0.8,0.8)));


    s.add_light_sphere(target + Point3::new(2.0, 1.5, -1.0),
                           50.0,
                           Color::new(1.0, 0.8, 0.6));


    s.add_mesh_object(target + Point3::new(-0.5, 2.0, 0.0),
                      "tests/triangles.obj",
                      Matte::create(Color::new(1.0, 0.0, 0.0)));


    s.add_sphere(target + Point3::new(2.0, 0.0, 0.0),
                     1.0,
                     Metal::create(Color::new(0.9, 0.9, 0.9 ), 0.0));


    let e = engine::Engine::new(s, width ,height,100, 0.2);
    e.compute(4);
}
